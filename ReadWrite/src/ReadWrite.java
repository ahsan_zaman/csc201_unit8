/**
 * @author Ahsan Zaman
 */
import java.io.IOException;
import java.io.RandomAccessFile;

import javax.swing.JOptionPane;


public class ReadWrite {
	private int studentID;
	private float studentGPA;
	
	public static final String FILEPATH="Stu.dat";
	
	public ReadWrite(){
		studentID=0;
		studentGPA=0f;
	}
	
	public ReadWrite( int studentID, float studentGPA ){
		this.studentID = studentID;
		this.studentGPA = studentGPA;
	}
	
	public String toString(){
		return studentID+" "+studentGPA;
	}
	
	public static void main( String[] args ) throws IOException{
		try{
			int studentID=0;
			float studentGPA=0f;
			ReadWrite[] obj = new ReadWrite[5];
			for( int i=0; i<5 ;i++ ){
				studentID = Integer.parseInt( JOptionPane.showInputDialog( "Enter ID for student #"+(i+1) ) );
				studentGPA = Float.parseFloat( JOptionPane.showInputDialog( "Enter gpa for student #"+(i+1) ) );
				obj[i] = new ReadWrite( studentID, studentGPA );
				writeToFile( FILEPATH, obj[i].toString(), -1 );
			}
			studentID = Integer.parseInt( JOptionPane.showInputDialog( "Enter Student ID to find gpa: " ) );
			} catch( IOException e){
			JOptionPane.showMessageDialog( null, "IOException thrown." );
		}
	}
	
	
	private static byte[] readFromFile( String filePath, int position, int size ) throws IOException {
		RandomAccessFile file = new RandomAccessFile( filePath, "rw" );
		file.seek( position );
		byte[] bytes = new byte[size];
		file.read( bytes );
		file.close();
		return bytes;
	}
	
	private static void writeToFile( String filePath, String data, int position )throws IOException {
		RandomAccessFile file = new RandomAccessFile(filePath, "rw");
		file.seek( position );
		file.write( data.getBytes() );
		file.close();}

}
