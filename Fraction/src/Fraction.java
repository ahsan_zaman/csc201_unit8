import java.io.Serializable;

public class Fraction implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int numerator;
	private int denominator;
	private static char slash;

	/**
	 * Default constructor for initializing class attributes.
	 */
	public Fraction() {
		super();
		Fraction.slash = '/';
		// TODO Auto-generated constructor stub
	}

	/**
	 * Overloaded constructor. Initializes attributes for class.
	 * @param numerator Value to store in numerator
	 * @param denominator Value to store in denominator
	 */
	public Fraction(int numerator, int denominator) {
		super();
		Fraction.slash = '/';
		this.numerator = numerator;
		this.denominator = denominator;
	}
	
	/**
	 * Numerator is returned. Getter method.
	 * @return numerator
	 */
	public int getNumerator() {
		return numerator;
	}

	/**
	 * Sets a value for the attribute numerator
	 * @param numerator Value to set the attribute numerator to
	 */
	public void setNumerator(int numerator) {
		this.numerator = numerator;
	}
	
	/**
	 * Returns denominator attribute
	 * @return denominator
	 */
	public int getDenominator() {
		return denominator;
	}

	/**
	 * Sets a value for the attribute denominator
	 * @param denominator Value to set the attribute denominator to
	 */
	public void setDenominator(int denominator) {
		this.denominator = denominator;
	}

	/**
	 * Returns slash character attribute
	 * @return Slash character
	 */
	public static char getSlash() {
		return slash;
	}

	/**
	 * Sets slash attribute
	 * @param slash will contain the character to set slash to
	 */
	public static void setSlash(char slash) {
		Fraction.slash = slash;
	}

	/**
	 * Returns class attributes in a string.
	 */
	public String toString() {
		return "Fraction [numerator=" + numerator + ", denominator="
				+ denominator + "]";
	}

}