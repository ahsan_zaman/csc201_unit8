/**
 * @author Ahsan Zaman
 * 
 * Purpose:
 * To create three Fraction class objects and store their attributes and write each object to a file name "SerialF.dat" 
 * and then to read that file and display it to user by storing in an object.
 * 
 * Algorithm:
 * 1. Create array of three Fraction objects
 * 2. Ask user to input values for the objects
 * 3. Create FileOutputStream object with the file "SerialF.dat"
 * 4. Create the ObjectOutputSteam object
 * 5. Write the attributes of the three objects into "SerialF.dat" using FileOutputStream object
 * 6. close the objects.
 * 7. Create FileInputStream object with the file "SerialF.dat"
 * 8. Create ObjectOutputStream object 
 * 9. Read from the file "SerialF.dat"
 * 10. Close the objects
 */
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.InputMismatchException;

import javax.swing.*;

public class FractionDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/**
		 * Fraction array of three objects. Attributes, numerator and denominator are inputed 
		 * by user and then written to a file.
		 * 
		 * Exception handling is used to get input, storing and writing values to a file.
		 */
		Fraction[] frac = new Fraction[3];
		int numerator=0, denominator=0;
		for( int i=0; i<3 ;i++ ){
			numerator = Integer.parseInt( JOptionPane.showInputDialog( "Give Numerator for fraction #"+(i+1) ) );
			denominator = Integer.parseInt( JOptionPane.showInputDialog( "Give Denominator for fraction #"+(i+1) ) );
			frac[i] = new Fraction( numerator, denominator);
		}
		try {
			FileOutputStream outFile = new FileOutputStream("SerialF.dat");
			ObjectOutputStream objOutput = new ObjectOutputStream(outFile);
			for( int i=0; i<3 ;i++ ){
				objOutput.writeObject( frac[i] );
			}
			objOutput.close();
			outFile.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog( null, "File Not Found! " );
		} catch (IOException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog( null, "IOException Thrown." );
		} catch(InputMismatchException e){
			JOptionPane.showMessageDialog( null, "Input mismatch exception. Please give correct integer input." );
		}

		try {
			/**
			 * Creating FileInputStream and ObjectInputStream objects with "Serial.dat" to attempt to read from that file.
			 * Storing the objects from the file into an array of objects and typcasting them to Fraction to display the attributes.
			 */
			FileInputStream inFile = new FileInputStream("SerialF.dat");
			ObjectInputStream objInput = new ObjectInputStream(inFile);
			Object[] obj = new Object[3];
			for( int i=0 ;i<3 ;i++ ){
				obj[i] = objInput.readObject();
			}
			objInput.close();
			inFile.close();
			
			JOptionPane.showMessageDialog( null, "File has been read:\n"+( Fraction ) obj[0]+"\n"+ (Fraction) obj[1]+"\n"+(Fraction) obj[2] );
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog( null, "File Not Found! " );
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog( null, "Class Not Found! " );
		} catch (IOException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog( null, "IOException Thrown." );
		}



	}

}