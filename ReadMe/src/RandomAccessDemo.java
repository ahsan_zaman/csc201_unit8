/**
 * @author Ahsan Zaman
 * 
 * Purpose:
 * To read specific characters from a file and display them to the user.
 * 
 * Algorithm:
 * 1. Create a RandomAccessFile Object using filepath and set it to read.
 * 2. Find the position to be read and read into a byte array. 
 * 3. Convert byte array to String and display it to the user.
 * END
 */
import java.io.IOException;
import java.io.RandomAccessFile;
import javax.swing.*;
public class RandomAccessDemo {

	public final static String FILEPATH = "Pledge.txt";
	public static void main( String[] args ) throws IOException{
		try{
			JOptionPane.showMessageDialog( null, "124th character: "+new String( readFromFile( FILEPATH, 124, 1 ) ) );
			JOptionPane.showMessageDialog( null, "135th character: "+new String( readFromFile( FILEPATH, 135, 1 ) ) );
		} catch( IOException e){
			JOptionPane.showMessageDialog( null, "IOException thrown." );
		} finally{
			
		}
	}
	
	/**
	 * Reads into a byte array from a txt file and returns the byte array.
	 * @param filePath The name and location of the file to be read
	 * @param position The position from which the file should be started to be read.
	 * @param size The number of characters to be read from file
	 * @return byte array is returned which contains the data read.
	 * @throws IOException In case there is this specific exception
	 */
	private static byte[] readFromFile( String filePath, int position, int size ) throws IOException {
		RandomAccessFile file = new RandomAccessFile(filePath, "r");
		file.seek(position);
		byte[] bytes = new byte[size];
		file.read(bytes);
		file.close();
		return bytes;
	}
}
